require('dotenv').config()

const config = require('./config')

require('./app').listen(config.port, () => {
    console.log(`Listening to port ${config.port}`)
    console.log(`http://localhost:${config.port}`)
})