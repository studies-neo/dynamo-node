# introduction

A simple API using amazon dynamodb via dynamoose, express, and a few other tools here and there

## dependencies

A full installation of nodejs

- nodejs
- npm
- npx

Along with

- docker
- docker compose plugin

## installing dependencies

    npm i

## running the database

Run it via docker

    docker run -p 8000:8000 -d amazon/dynamodb-local

##  running the project

Prep the env beforehand:

    export AWS_ACCESS_KEY_ID="Your AWS Access Key ID"
    export AWS_SECRET_ACCESS_KEY="Your AWS Secret Access Key"
    export AWS_REGION="us-east-1"

via npm

    npm start

via node

    node .

via docker

    docker build -t dynamo-demo:latest .
    docker run -p 3000:3000 dynamo-demo:latest