const Route = require('express').Router
const taskService = require('../service/task-service')

const route = Route()

route.get('/', async (req, res) => {
    res.json(await taskService.findAll())
})

route.get('/:id', async (req, res) => {
    const found = await taskService.findById(req.params.id)
    if(!found) res.status(404)
    res.json(found)
})

route.post('/', async (req, res) => {
    res.json(await taskService.save(req.body))
})

route.put('/:id', async (req, res) => {
    res.json(await taskService.update({...req.body, id : req.params.id}))
})

route.delete('/:id', async (req, res) => {
    const found = await taskService.delete(req.params.id)
    if(!found) res.status(404)
    res.json(found)
})

module.exports = route
