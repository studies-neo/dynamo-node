FROM node:18-alpine3.16

RUN mkdir -p '/opt/app'
WORKDIR /opt/app

# move all the files into the container
COPY data ./data
COPY routes ./routes
COPY service ./service
COPY *.js ./
COPY *.json ./
COPY .env ./

# install the dependencies
RUN npm install

EXPOSE 3000

CMD [ "node", "." ]