curl -X POST http://localhost:3000/task \
  -H 'content-type: application/json' \
  --data '{ 
    "name" : "testing this shindig"
  }'

curl -X GET http://localhost:3000/task

curl -X GET http://localhost:3000/task/c106c583-6e58-4085-8d4a-8fc924eef9b4

curl -X PUT http://localhost:3000/task/c106c583-6e58-4085-8d4a-8fc924eef9b4 \
  -H 'content-type: application/json' \
  --data '{ 
  "name" : "hello",
  "done" : true
}'

curl -X DELETE http://localhost:3000/task/c106c583-6e58-4085-8d4a-8fc924eef9b4