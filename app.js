const express = require("express");
const bodyParser = require("body-parser")
const morgan = require("morgan")
const cors = require('cors')

const app = express()
app.use(cors())
app.use(bodyParser.json({ limit : "10mb" }))
app.use(morgan("common"))

const fs = require('fs')
const path = require('path')

// hook up the routes
fs.readdirSync('./routes').forEach(routes => {
    const route = require(path.join(__dirname, 'routes', routes))
    if(routes == 'index.js') {
        console.log(`Routing index.js to path /`)
        app.use(route)
    } else {
        const base = path.parse(routes).name
        console.log(`Routing ${routes} to path /${base}`)
        app.use(`/${base}`, route)
    }
})

module.exports = app
