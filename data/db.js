const dynamoose = require("dynamoose");
const config = require('../config')

// Create DynamoDB instance
dynamoose.aws.ddb.local(config.dynamo.host);

/**
 * Schemas
 */
const schema = {
    Task : new dynamoose.Schema({
        id : String,
        name : String,
        done: Boolean,
        description: {
            type : String,
            required: false
        },
    }, {
        timestamps : {
            "createdAt": "createdAt",
            "updatedAt": "updatedAt"
        }
    })
}

/**
 * Models
 */
const models = {
    Task : dynamoose.model("Task", schema.Task)
}

/**
 * Table
 */
const table = new dynamoose.Table("AppTable", [ models.Task ])

/**
 * output
 */
module.exports = {
    dynamoose,
    schema,
    models,
    table
}