const { randomUUID } = require('crypto')
const { models } = require('../data/db')

const { Task } = models

module.exports = {
    /**
     * returns a list of people
     * @returns list of people
     */
    async findAll() {
        return await Task.scan().exec()
    },
    /**
     * saves a person in the dynamo db
     * @param {{name : string, description: string?}} task object of the person being saved
     * @returns { name : string, id: string}
     */
    async save(task) {
        if(!task) return null
        const toSave = {
            id : randomUUID(),
            name : task.name,
            description: task.description,
            done: false
        }
        await Task.create(toSave)
        return toSave
    },
    /**
     * updates a person
     * @param {{id : string, name : string, description: string?, done: boolean?}} task updates a person
     * @returns the updated object
     */
    async update(task) {
        if(!task || !task.id) return null
        await Task.update(task.id, {
            name : task.name,
            description: task.description,
            done: !!task.done
        })
        return this.findById(task.id)
    },
    /**
     * find an individual by id
     * @param {string} id finds an object by id
     * @returns a person object
     */
    async findById(id) {
        return await Task.get(id)
    },
    /**
     * delete by id
     * @param {string} id id to search for
     * @returns the deleted object
     */
    async delete(id) {
        const found = await Task.get(id)
        if(found) {
            await found.delete()
            return found.toJSON()
        }
        return null
    }
}