
const port = Number(process.env.PORT || 3000)
const dynamoHost = process.env.DYNAMO_URL || "http://localhost:8000"

module.exports = {
    port,
    dynamo: {
        host: dynamoHost
    }
}